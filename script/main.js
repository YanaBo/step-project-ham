$(document).ready(function () {
  // Tabs
  const $tabs = $('.tabs');
  const $tabTitle = $('.tabs-title');
  const $tabText = $('.tabs-text');

  $tabs.on('click', changingTab);

  $tabTitle.first().addClass('active');
  $tabText.first().css({'display': 'block'});

  function changingTab(event) {
    const $target = $(event.target);
    const dataTab = $target.data('tab');
    const targetClass = $target.attr('class');

    if (targetClass === 'tabs-title') {
      for (let i = 0; i < $tabTitle.length; i++) {
        $tabTitle.eq(i).removeClass('active');
      }

      $target.addClass('active');

      for (let i = 0; i < $tabText.length; i++) {
        if (dataTab === i) {
          $tabText.eq(i).css({'display': 'block'});
        } else {
          $tabText.eq(i).css({'display': 'none'});
        }
      }
    }
  }

  // Slider
  $('.reviews-slider').slick({
    speed: 300,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    prevArrow: "<span class='control-s control-prev slick-prev'><i class='fa fa-chevron-left'></i></span>",
    nextArrow: "<span class='control-s control-next slick-next'><i class='fa fa-chevron-right'></i></span>",
    asNavFor: '.reviews-slider-nav'
  });
  $('.reviews-slider-nav').slick({
    slidesToShow: 4,
    slidesToScroll: 1,
    asNavFor: '.reviews-slider',
    focusOnSelect: true,
  });

  // Filter
  const $portfolioTabsTitle = $(".portfolio-tabs-title");
  const $portfolioItem = $('.portfolio-list-item');
  const $loadMore = $('.load-more');

  $portfolioTabsTitle.on('click', function () {
    const dataFilter = $(this).attr('data-filter');

    $loadMore.hide();
    $(this).addClass("active");
    $(this).siblings().removeClass('active');

    if (dataFilter === "all") {
      $portfolioItem.hide();
      $portfolioItem.slice(0, 12).show();
      $loadMore.show();
    } else {
      $portfolioItem.not('.filter-' + dataFilter).hide('3000');
      $portfolioItem.filter('.filter-' + dataFilter).show('3000');
    }
  });

  // Load more
  $portfolioItem.slice(0, 12).show();
  $loadMore.on('click', function (e) {
    const $hidden = $portfolioItem.filter(':hidden');
    e.preventDefault();
    $hidden.slice(0, 12).slideDown();
    if($portfolioItem.filter(':hidden').length === 0) {
      $loadMore.fadeOut('slow')
    }
  });
});
